
import { Application, Router } from "https://deno.land/x/oak/mod.ts";
import router  from "./routes/index.routes.ts";
const app = new Application();
router.get("/",(ctx)=>{
ctx.response.body = "hello word"
});
app.use(router.routes());
//im´portamos todos los metodo del router
app.use(router.allowedMethods());
console.log('server on port ', 3000)
await app.listen({port:3000});
