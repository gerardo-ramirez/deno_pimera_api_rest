;
import { Router } from "https://deno.land/x/oak/mod.ts";
import *as indexCtrl from "../controllers/index.controllers.ts";

const router = new Router();
router.get("/",(ctx)=>{
ctx.response.body = "hello word"
});
router.get("/users", indexCtrl.getUsers);
router.get("/users/:id", indexCtrl.getUser);
router.post("/users", indexCtrl.createUsers);
router.delete("/users/:id", indexCtrl.deleteUsers);
router.put("/users/:id", indexCtrl.updateUsers);




export default router; 