import { Response, Request, Body } from "https://deno.land/x/oak/mod.ts";
import { v4 } from "https://deno.land/std/uuid/mod.ts";
interface User {
    id: string,
    name: string
}
interface userBody{
    name: string
}
let users: User[] = [{
    id: "1",
    name: "Raquel Español"
}];

export const getUsers = ({response}:{response: Response})=>{
response.body ={
    message: "successful query",
    users
}
};
export const getUser =async ({response, params}:{response:Response, params: {id: string}})=>{
  const userFound = users.find(user => user.id === params.id);
  if(userFound){
      response.status = 200;
      response.body = {
          message: "un usuario",
          userFound
      }

  }else{
      response.status = 404;
      response.body = {
          message: "user no encontrado"
      }
  }
};
export const createUsers =async  ({response, request}:{response:Response, request: Request})=>{
   const body : Body = await  request.body();
   if(!request.hasBody){
       response.status = 404;
       response.body ={
           message: "body is required"
       }
   }else{
    const newUser: userBody = body.value;
    users.push({
        id: v4.generate(),
        name: newUser.name,
    })
    response.status= 200;
    response.body={
        message: "new user created",
        newUser
    }
}
  
};
export const updateUsers = async ({request, response, params}:{response:Response, request: Request,params: {id: string}})=>{
  const userFound = users.find(user => user.id === params.id);
if(!userFound){

    response.status = 404;
    response.body ={
        message: "user not found"
    }
}else{
    const body = await request.body();
    const updateUser = body.value;
    users = users.map(user => user.id === params.id ? {...user, ...updateUser}: user);
    response.status = 200 ;
    response.body = {
        
            message: "user update",
            users
        
    }
}

};
export const deleteUsers = ({response, params}:{response:Response, params: {id: string}})=>{
     users = users.filter(user => user.id !== params.id);
    

        response.status = 200;
      response.body = {
          message: "user delete",
          users
      }
    
};

